
export default () => {
  
  // ======================
  // PUERTO
  // ======================
  process.env.PORT = process.env.PORT || String(3000);

  // ======================
  // ENTORNO
  // ======================

  process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

  // ======================
  // Base de DATOS
  // ======================
  process.env.URLDB = (process.env.NODE_ENV === 'dev') ? 'mongodb://@localhost:27017/buscador_rick' : process.env.MONGO_URI;

  // ======================
  // Vencimiento del token
  // ======================
  // segundos
  // minutos
  // horas
  // dias

 

  process.env.SEED = process.env.SEED || 'seed-desarrollo';

  // ======================
  // Google client ID
  // ======================

  process.env.CLIENT_ID = process.env.CLIENT_ID || '1017629132763-lq6sippcb7rugmt00msklempo2ogjofg.apps.googleusercontent.com';
}
