
// libreria para la bse de datos
import * as mongoose from 'mongoose';

const uniqueValidator = require('mongoose-unique-validator');
let Schema = mongoose.Schema;


let DocumentSchema = new Schema({
  title: {
    type: String,
    // unique: true,
  },

  description: {
    type: String,
  },

  tokens: {
    type: String,
  },

  root: {
    type: String,
  },

  state: {
    type: Boolean,
    default: true
  }
});

/*
DocumentSchema.methods.toJSON = function () {
  let content = this;
  let contentObject = content.toObject();
  delete contentObject.content;
  return contentObject;
};
*/
DocumentSchema.plugin(uniqueValidator, {message: '{PATH} debe ser unico'});
module.exports = mongoose.model('Document',  DocumentSchema);
