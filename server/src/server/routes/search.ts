import { Router, Request, Response } from 'express';


const searchRouter = Router()
const _ = require('underscore')
const DocumentSchema = require('../models/document');

import {indexacion} from '../helpers/indexacion';

// controlador para hacer la busqueda
searchRouter.post('/search', (req: Request, res: Response) => {
  
  let tokenParams = req.body.token;
  DocumentSchema.find({state: true}, 'title description tokens root state')
    .exec((err, documents) => {
      if (err) {
        return res.status.json({
          ok: false,
          err
        })
      }
 
      res.json({
        ok: true,
        data: {
          documents: indexacion(documents, [tokenParams])
        }
      })
    })
  });
  
export default searchRouter;