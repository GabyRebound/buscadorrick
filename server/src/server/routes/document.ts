import { Router, Request, Response } from 'express';

// scraping para el body
import { JSDOM } from 'jsdom';

// libreria para interactuar con archivos
import * as fs from "fs";

// scraping para los meta tags
const extractor = require('unfluff');

const documentRouter = Router()
const _ = require('underscore')
const DocumentSchema = require('../models/document');
import { depuracion, select } from '../helpers/helper';
var uniqid = require('uniqid');

// controlador para obtener todos los documentos
documentRouter.get('/document', (req: Request, res: Response) => {
  let desde = Number(req.query.desde) || 0 ;
  let limit = Number(req.query.limit) || 5 ;

  DocumentSchema.find({state: true}, 'title description tokens root state')
    .exec((err, documents) => {
      if (err) {
        return res.status.json({
          ok: false,
          err
        })
      }
      DocumentSchema.count({state: true}, (errC, nroDocument) => {
        res.json({
          ok: true,
          data: {
            documents,
            nroContent: nroDocument
          }
        })
      })
    })
});




// controlador para insertar documentos desde en Angular
documentRouter.post('/document-file', async (req: Request, res: Response) => {
  
  // capturamos el archivo enviado por el formulario
  let EDFile = req.files.file;

  // generamos un ID unico
  let idUnique = uniqid();
  
  //  genramos el nombre del archivo con el cual se va a guardar
  let nameFile = `${idUnique}-${EDFile.name}`;

  // guardamos el archivo
  await EDFile.mv(`./src/server/public/documents/${nameFile}`,err => {
      if(err) return res.status(500).send({ message : err })
  })

  // Leemoe el contenido del archivo html
  fs.readFile(`./src/server/public/documents/${nameFile}`, 'utf-8', async (err, data) => {
    if(err) {
      if(err) return res.status(500).send({ message : err })
    } else {
      
      // es una funcion para sacar titulo, descripcion con scrping
      let extractionMeta = await extractor(data);

      // la variable para hacer scraping
      const dom = new JSDOM(data);

      // sacamos el body 
      let bodyHtml  = await dom.window.document.body;

      // ignoramos las etiquetas Scripts con scrpaing
      const selection = select(
        bodyHtml,
        element => {
          return element.tagName != 'SCRIPT';
        }
      );

      
      // procesos de depuracion
      let tokens = await depuracion(selection).join(' ');      

      // creamo la coleccion para la base de datos
      let document = new DocumentSchema({
        title: extractionMeta.title,
        description: extractionMeta.description,
        root: `http://localhost:3000/documents/${nameFile}`,
        tokens: tokens
      });
    
      // metodo save para guardar un user en mondoDB
      document.save((err, documentDB) => {
        if (err) {
          return res.status(400).json({
            ok: false,
            err
          });
        }
        res.json({
          ok: true,
          data: {
            document: documentDB
          }
        });
      });
    }
  });
});



export default documentRouter
