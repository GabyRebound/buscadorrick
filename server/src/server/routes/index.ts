import Server from '../server';
import * as express from 'express';
import documentRouter from './document';
import searchRouter from './search';

const server: express.Application = express();

server.use(documentRouter);
server.use(searchRouter);

export default server;
