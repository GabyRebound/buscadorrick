const fs = require('fs');
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const natural = require('natural');

// recorrido de forma recursiva del DOM para sacar el body del HTML
export const select = (element, condition) => {
  let found = [];
  if (condition(element) && element.children.length===0 ) {
    found.push(element.textContent);
  }
  
  for (let i = 0; i < element.children.length; i++) {
    found = found.concat(select(element.children[i], condition));
  }
  return found;
};

// borrar los parrafos falsos donde hay caractere raros cuenta de texto

export const eliminarParrafosFalsos = (palabra) => {
  let caracteresRaros = ['', ' ', '\n', '\t', '|', '&', '(', ')', '[', ']'];
  return caracteresRaros.includes(palabra);
}
// eliminamos parrafos vacios, es decir donde no hay nada de texto y espacios en blanco
export const vacias = (palabra) => {
  for(let i=0; i<palabra.length; i++ ) {
    if(palabra[i] !== ' ') {
      return false;
    }
    return true;
  }
}
// eliminamos parrafos donde hay saltos de linea y tabulaciones cuenta de texto
export const saltos = (palabra) => {
  for(let i=0; i<palabra.length; i++) {
    if(palabra[i] !== '\n' && palabra[i] !== '\t') {
      return false;
    }
    return true;
  }
}

// eliminamo parrafos que empiezen con http o https, para evitar enlaces
export const deleteHttp = (element) =>  element.startsWith('http')

// eliminamo eiquetas que sobraron
export const deleteEtiq = (element) =>  element.startsWith('<')

// metodo que invoca la depuracion de los parrafos
export const deleteElementEmpty = ( contenido ) => {
  let aux = [];
  contenido.forEach(element => {
    if( !eliminarParrafosFalsos(element) && !vacias(element) && !saltos(element) && !deleteHttp(element) && !deleteEtiq(element) ) {
      aux.push(element);
    }
  });
  
  return aux;
}


// Unimos todos los parrafos en uno solo para empezar a tokenizar, ..etc

export const unirParrafo = (arrayParrafos) => {
  return arrayParrafos.join(' ');
}


// Tokenizamos el contenido en bruto de la Pagina
export const tokenizacion = (parrafos) => {
  var tokenizer = new natural.AggressiveTokenizerEs();

  // eliminamos espacios de los costados y espacios en blanco duplicados
  let paso1 = parrafos.split(/\s+/) .join(' ').trim();

  // Metodo que realiza la Tokenizacion en un Array donde esta todos los tokens
  let paso2 = tokenizer.tokenize(paso1);

  return paso2
}

// Stemming and Lemmatization el array de tokens
export const stemming = (arrayPalabras) => arrayPalabras.map((element) => natural.PorterStemmerEs.stem(element) )


// eliminamos tokens que contengan caracteres raros como ser estos
export const eliminarSignosDePuntuacion = (palabra) => {
  let aux = '';
  let signos = [ '@', '!', '%', '&', '\'', '\"', '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '[', '{', '<', '\'', '|', '=', ']', '}', '>', '^', '~', '?', ']', '_']
  palabra.split('').forEach(caracter => {
    if (!signos.includes(caracter)) {
      aux+=caracter;
    }
  });
  return aux
}

// Reemplazamos en los tokens caracteres con acentuacion por unos del alfabeto
export const reemplazarCaracteresEspeciales = (palabra) => {
  let aux = '';

  let as = ['ā','À','Á','Â','Ã','Ä','Å','à','á','â','ã','ä','å','æ','Ā','Ǻ','Ǽ','Ă','ǽ','ă','Ą','ą', 'Æ','á','Á','à','À','â','Â','ä','Ä']
  let bs = ['ß']
  let cs = ['Ć','ć','Ĉ','ĉ','Ċ','ċ','Č','Ç','č' ,'ç']
  let ds = ['Ď','ď','Đ','đ','Ð']
  let es = ['é','É','è','È','ê','Ê','ë','Ë','È','Œ','É','œ','Ê','Ë','Ē','ē','Ĕ','ĕ','Ė','ė','Ę','ę','Ě','ě','è','é','ê','ë']
  let fss = ['Ŧ','ſ','ƒ']
  let gs = ['Ĝ','ĝ','Ğ','ğ','Ġ','ġ','Ģ','ģ']
  let hs = ['Ĥ','ĥ','Ħ','ħ']
  let is = ['í','Í','ì','Ì','î','Î','ï','Ï','Ì','Í','Î','Ï','Ĩ','Ī','ĭ','Į','į','ì','İ','í','ı','î','ï','ĳ','ĺ']
  let js = ['Ĵ','ĵ']
  let ks = ['Ķ','ķ']
  let ls = ['Ĺ','Ľ','ľ','Ŀ']
  let ns = ['ŉ','Ņ','ņ']
  let ñs = ['ň','Ñ','Ń','ń','Ň','ñ']
  let os = ['ó','Ó','ò','Ò','ô','Ô','ö','Ö','Ō','ō','Ŏ','ŏ','Ő','ő','Ò','Ó','Ô','Õ','Ö','Ø','ó','ô','õ','ø','Ơ','Ǿ','ǿ','ò','ö','ơ']
  let rs = ['Ŕ','ŕ','Ŗ','Ř','ř','ŗ']
  let ss = ['ś','Ś','Ŝ','ŝ','Ş','ş','Š','š']
  let ts = ['Ţ','ţ','Ť','ť','ŧ','ĩ','ī','Ĭ']
  let us = ['ú','Ú','ù','Ù','û','Û','ü','Ü','Ù','Ú','Û','Ü','Ũ','ũ','Ū','ū','Ŭ','ŭ','Ů','ů','Ű','ű','Ų','ų','Ĳ','ù','ú','ü','Ư','ư']
  let ws =  ['Ŵ','ŵ']
  let ys = ['Ý','Ŷ','Ÿ','ý','ÿ','ŷ']
  let zs = ['Ź','ź','ż','Ž','ž']


  palabra.split('').forEach(caracter => {
    if (as.includes(caracter)) {
      caracter = 'a'
    }
    if (bs.includes(caracter)) {
      caracter = 'b'
    }
    if (cs.includes(caracter)) {
      caracter = 'c'
    }
    if (ds.includes(caracter)) {
      caracter = 'd'
    }
    if (es.includes(caracter)) {
      caracter = 'e'
    }
    if (fss.includes(caracter)) {
      caracter = 'f'
    }
    if (gs.includes(caracter)) {
      caracter = 'g'
    }
    if (hs.includes(caracter)) {
      caracter = 'h'
    }
    if (is.includes(caracter)) {
      caracter = 'i'
    }
    if (js.includes(caracter)) {
      caracter = 'j'
    }
    if (ks.includes(caracter)) {
      caracter = 'k'
    }
    if (ls.includes(caracter)) {
      caracter = 'l'
    }
    if (ns.includes(caracter)) {
      caracter = 'n'
    }
    if (ñs.includes(caracter)) {
      caracter = 'ñ'
    }
    if (os.includes(caracter)) {
      caracter = 'o'
    }
    if (rs.includes(caracter)) {
      caracter = 'r'
    }
    if (ss.includes(caracter)) {
      caracter = 's'
    }
    if (ts.includes(caracter)) {
      caracter = 't'
    }
    if (us.includes(caracter)) {
      caracter = 'u'
    }
    if (ws.includes(caracter)) {
      caracter = 'w'
    }
    if (ys.includes(caracter)) {
      caracter = 'y'
    }
    if (zs.includes(caracter)) {
      caracter = 'z';
    }
    aux += caracter;
  });

  return aux ;
}

// convertimos los tokens a minuscullas e invocamos a lo metodos mensionados
export const minisculaSignosPuntuacionAcentos = (arrayPalabras) => {
  let aux = []
  arrayPalabras.forEach(palabra => {
    let paso1 = palabra.toLowerCase();
    let paso2 = eliminarSignosDePuntuacion(paso1);
    let paso3 = reemplazarCaracteresEspeciales(paso2)
    aux.push(paso3)
  });
  return aux;
}

// Una vez tratados los tokens, procedemos descartar los tokens con palabras vacias
export const eliminarPalabraVacias = (arrayPalabras) => {
  let palabras_vacias_es = ['el','la','los','les','las','de','del','a','ante','con','en','para','por','y','o','u','tu','te','ti','le','que','al','ha','un','han','lo','su','una','estas','esto','este','es','tras','suya','a','acá','ahí','ajena','ajenas','ajeno','ajenos','al','algo','algún','alguna','algunas','alguno','algunos','allá','alli','allí','ambos','ampleamos','ante','antes','aquel','aquella','aquellas','aquello','aquellos','aqui','aquí','arriba','asi','atras','aun','aunque','bajo','bastante','bien','cabe','cada','casi','cierta','ciertas','cierto','ciertos','como','cómo','con','conmigo','conseguimos','conseguir','consigo','consigue','consiguen','consigues','contigo','contra','cual','cuales','cualquier','cualquiera','cualquieras','cuancuán','cuando','cuanta','cuánta','cuantas','cuántas','cuanto','cuánto','cuantos','cuántos','de','dejar','del','demás','demas','demasiada','demasiadas','demasiado','demasiados','dentro','desde','donde','dos','el','él','ella','ellas', 'ello','ellos','empleais','emplean','emplear','empleas','empleo','en','encima','entonces','entre','era','eramos','eran','eras','eres','es','esa','esas','ese','eso','esos','esta','estaba','estado','estais','estamos','estan','estar','estas','este','esto','estos','estoy','etc','fin','fue','fueron','fui','fuimos','gueno','ha','hace','haceis','hacemos','hacen','hacer','haces','hacia','hago','hasta','incluso','intenta','intentais','intentamos','intentan','intentar','intentas','intento','ir','jamás','junto','juntos','la','largo','las','lo','los','mas','más','me','menos','mi','mía','mia','mias','mientras','mio','mío','mios','mis','misma','mismas','mismo','mismos','modo','mucha','muchas','muchísima','muchísimas','muchísimo','muchísimos','mucho','muchos','muy','nada','ni','ningun','ninguna','ningunas','ninguno','ningunos','no','nos','nosotras','nosotros','nuestra','nuestras','nuestro','nuestros','nunca','os','otra','otras','otro','otros','para','parecer','pero','poca','pocas','poco','pocos','podeis','podemos','poder','podria','podriais','podriamos','podrian','podrias','por','por','qué','porque','primero','primero','desde','puede','pueden','puedo','pues','que','qué','querer','quien','quién','quienes','quienes','quiera','quienquiera','quiza','quizas','sabe','sabeis','sabemos','saben','saber','sabes','se','segun','ser','si','sí','siempre','siendo','sin','sín','sino','so','sobre','sois','solamente','solo','somos','soy','sr','sra','sres','esta','su','sus','suya','suyas','suyo','suyos','tal','tales','también','tambien','tampoco','tan','tanta','tantas','tanto','tantos','te','teneis','tenemos','tener','tengo','ti','tiempo','tiene','tienen','toda','todas','todo','todos','tomar','trabaja','trabajais','trabajamos','trabajan','trabajar','trabajas','trabajo','tras','tú','tu','tus','tuya','tuyo','tuyos','ultimo','un','una','unas','uno','unos','usa','usais','usamos','usan','usar','usas','uso','usted','ustedes','va','vais','valor','vamos','van','varias','varios','vaya','verdad','verdadera','vosotras','vosotros','voy','vuestra','vuestras','vuestro','vuestros','y','ya','yo','como','cómo','hacer','se','tengo', '','y', 'son',];
  let palabras_vacias_en = ['a', 'about', 'above', 'above', 'across', 'after', 'afterwards', 'again', 'against', 'all', 'almost', 'alone', 'along', 'already', 'also', 'although', 'always', 'am', 'among', 'amongst', 'amoungst', 'amount',  'an', 'and', 'another', 'any', 'anyhow', 'anyone', 'anything', 'anyway', 'anywhere', 'are', 'around', 'as',  'at', 'back', 'be', 'became', 'because', 'become', 'becomes', 'becoming', 'been', 'before', 'beforehand', 'behind', 'being', 'below', 'beside', 'besides', 'between', 'beyond', 'bill', 'both', 'bottom', 'but', 'by', 'call', 'can', 'cannot', 'cant', 'co', 'con', 'could', 'couldnt', 'cry', 'de', 'describe', 'detail', 'do', 'done', 'down', 'due', 'during', 'each', 'eg', 'eight', 'either', 'eleven', 'else', 'elsewhere', 'empty', 'enough', 'etc', 'even', 'ever', 'every', 'everyone', 'everything', 'everywhere', 'except', 'few', 'fifteen', 'fify', 'fill', 'find', 'fire', 'first', 'five', 'for', 'former', 'formerly', 'forty', 'found', 'four', 'from', 'front', 'full', 'further', 'get', 'give', 'go', 'had', 'has', 'hasnt', 'have', 'he', 'hence', 'her', 'here', 'hereafter', 'hereby', 'herein', 'hereupon', 'hers', 'herself', 'him', 'himself', 'his', 'how', 'however', 'hundred', 'ie', 'if', 'in', 'inc', 'indeed', 'interest', 'into', 'is', 'it', 'its', 'itself', 'keep', 'last', 'latter', 'latterly', 'least', 'less', 'ltd', 'made', 'many', 'may', 'me', 'meanwhile', 'might', 'mill', 'mine', 'more', 'moreover', 'most', 'mostly', 'move', 'much', 'must', 'my', 'myself', 'name', 'namely', 'neither', 'never', 'nevertheless', 'next', 'nine', 'no', 'nobody', 'none', 'noone', 'nor', 'not', 'nothing', 'now', 'nowhere', 'of', 'off', 'often', 'on', 'once', 'one', 'only', 'onto', 'or', 'other', 'others', 'otherwise', 'our', 'ours', 'ourselves', 'out', 'over', 'own', 'part', 'per', 'perhaps', 'please', 'put', 'rather', 're', 'same', 'see', 'seem', 'seemed', 'seeming', 'seems', 'serious', 'several', 'she', 'should', 'show', 'side', 'since', 'sincere', 'six', 'sixty', 'so', 'some', 'somehow', 'someone', 'something', 'sometime', 'sometimes', 'somewhere', 'still', 'such', 'system', 'take', 'ten', 'than', 'that', 'the', 'their', 'them', 'themselves', 'then', 'thence', 'there', 'thereafter', 'thereby', 'therefore', 'therein', 'thereupon', 'these', 'they', 'thickv', 'thin', 'third', 'this', 'those', 'though', 'three', 'through', 'throughout', 'thru', 'thus', 'to', 'together', 'too', 'top', 'toward', 'towards', 'twelve', 'twenty', 'two', 'un', 'under', 'until', 'up', 'upon', 'us', 'very', 'via', 'was', 'we', 'well', 'were', 'what', 'whatever', 'when', 'whence', 'whenever', 'where', 'whereafter', 'whereas', 'whereby', 'wherein', 'whereupon', 'wherever', 'whether', 'which', 'while', 'whither', 'who', 'whoever', 'whole', 'whom', 'whose', 'why', 'will', 'with', 'within', 'without', 'would', 'yet', 'you', 'your', 'yours', 'yourself', 'yourselves', 'the'];
  
  let aux = [];
  let regex = /[0-9]+/
  arrayPalabras.forEach(palabra => {
    if(!palabras_vacias_es.includes(palabra) && !palabras_vacias_en.includes(palabra) && !regex.exec(palabra) && palabra.length>2 ) {
      aux.push(palabra);
    }
  });
  return aux;
}


// Funcion que invoca a todos las funciones mensionadas
export const depuracion = (selection) => {

   // Proceso de depuración (Depuración y supresión de código fuente e Identificación de casos especiales)
   let selection1 = deleteElementEmpty(selection);

   // unimos un parrafo
   let selection2 = unirParrafo(selection1);


   // =========== Normalización de textos==========
   // Tokenizacion
   let selection3 = tokenizacion(selection2);

   // Conversión a minúsculas, eliminación de signos de puntuación y acentos
   let selection4 = minisculaSignosPuntuacionAcentos(selection3);

   // Stemming and Lemmatization cada token
   let selection5 = stemming(selection4);
   
   // Eliminación de palabras vacías
   let selection6 = eliminarPalabraVacias(selection5);

   return selection6;
}