import { depuracion } from '../helpers/helper';

// modelo vetorial
// recien los documentos de la base de datos y tambien la texto a buscas
export const indexacion = (datos, token) => {

  //console.log(depuracion(token).join(' '));

  // documento 8
  datos.push({
    title: 'consulta',
    tokens: depuracion(token).join(' ')
  });

  // agarrar todos los terminos y todos los documentos
  const repetidos = (element, array) => {
    let cont = 0;
    var idx = array.indexOf(element);
    while (idx != -1) {
      cont++;
      idx = array.indexOf(element, idx + 1);
    }
    return cont;
  }


  // logaritmo en base X
  function getBaseLog(x, y) {
    return Math.log(y) / Math.log(x);
  }

  // array de terminos y documentos
  let terminos = []
  let documentos = []
  datos.forEach(element => {
    documentos.push(element.title);
    const tokens = element.tokens;
    tokens.split(' ').forEach(elementToken => {
      if( !terminos.includes(elementToken)) {
        terminos.push(elementToken);
      }
    });
  });

  let matrizPeso = {}
  
  // creamos la matriz de pesos con ceros
  terminos.forEach(elementTerm => {
    let docs = {}
    documentos.forEach(elementDoc => {
      docs[elementDoc] = 0;
    });
    matrizPeso[elementTerm] = docs;
  });

  let matrizTFIDF = JSON.parse(JSON.stringify(matrizPeso));
  
  // colocamos pesos a la matriz de pesos
  terminos.forEach(elementTerm => {
    documentos.forEach(elementDoc => {
      let token = datos.filter(elemento => elemento.title === elementDoc)[0].tokens.split(' ')
      matrizPeso[elementTerm][elementDoc] = repetidos(elementTerm, token);
    });
  });

  // Hayamos la matriz TF-IDF
  terminos.forEach(elementTerm => {
    documentos.forEach(elementDoc => {
      let tf =  matrizPeso[elementTerm][elementDoc];
      let N = documentos.length;
      let df = 0;
      
      Object.values(matrizPeso[elementTerm]).forEach(element => {
        let coincidencia = (element > 0) ? 1 : 0;
        df+=coincidencia;  
      });

      let TFIDF = tf* getBaseLog(10, (N/df));
      
      matrizTFIDF[elementTerm][elementDoc] = (TFIDF == NaN)? 0: TFIDF;
      
    });
  });

  // Hayamos la Similitud
  let similitud = []
  documentos.forEach(elementDoc => {
    let cont = 0;
    terminos.forEach(elementTerm => {
      if(matrizTFIDF[elementTerm][elementDoc] !== 0 &&  matrizPeso[elementTerm].consulta !==0 ) {
        cont += (matrizTFIDF[elementTerm][elementDoc] * matrizPeso[elementTerm].consulta)
      }
    });
    
    if(elementDoc !== 'consulta') {
      let documentoRes = datos.filter((item) => item.title === elementDoc)[0];

      similitud.push({
        sim: cont,
        document: documentoRes
      });
    }
  });

  // Ordenamos la Similitud
  similitud.sort((a, b) => (b.sim - a.sim))

  // filtramos solo las similitudes mayores que 0
  let respSim = similitud.filter(element => (element.sim > 0.000000000000000));

  // retornamos la Similitud
  return respSim;

}
