


// body de los formularios
import * as bodyParser from "body-parser";

import * as express from "express";

import setup from './config/config';

// libreira pra manejar mongoDB
import * as mongoose from 'mongoose';

// para que angular pueda hacer peticiones en desarrollo
import * as cors from 'cors';

// clase para saber  las rutas de los archivos HTML
const path = require('path')

// Pasa subir archivos
const fileUpload = require('express-fileupload')


export default class Server {

  public app: express.Application;
  public port: number;

  public static bootstrap(): Server {
    return new Server();
  }

  constructor () {
    // configura todo lo necesario, puerto, y otras variables como la URL de mongo
    this.config();

    // intanceamos express
    this.app = express();

    // parse application/x-www-form-urlencoded
    this.app.use(bodyParser.urlencoded({ extended: true }));
    // parse application/json
    this.app.use(bodyParser.json());
    // usanmos cors
    this.app.use(cors({origin: '*'}));

    this.app.use(fileUpload())

    // habilitar la carpeta public como publica para mostrar los archivos html guardados
    console.log(path.resolve(__dirname, './public'));
    this.app.use(express.static(path.resolve(__dirname, './public')))

    // nos conectamos ala base de datos
    mongoose.connect(process.env.URLDB, (err, resp) => {
      if (err) {
        throw err;
      }
      console.log('Mongo corriendo de forma correcta');
    })

  }

  config () {
      setup();
      this.port = Number(process.env.PORT);
  }

  start () {
    // express que corrar bajo un puerto
    this.app.listen(this.port, () => {
        console.log(`corriendo en el puerto: ${this.port}`);
    });
  }
}