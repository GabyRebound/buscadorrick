import Server from './server/server';
import routes from './server/routes';

const server: Server = Server.bootstrap();

server.app.use(routes);

server.start();