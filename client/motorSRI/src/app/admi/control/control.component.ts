import { Component, OnInit } from '@angular/core';
import { PagesService } from '../../../app/pages.service';
import { CargaImagenService } from '../../carga-imagen.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.css']
})
export class ControlComponent implements OnInit {
  lista:any[]=[];
  public respuestaImagenEnviada;
  public resultadoCarga;
  public nombreArchivo;
  constructor( private servicio: PagesService, private enviandoImagen: CargaImagenService, private router:Router) {
    this.lista = this.servicio.mostrarPaginas();
   }

  ngOnInit() {
  }

  public cargandoNombre(files: FileList){
    document.querySelector('#nombreArchivo').textContent = files[0].name;
    console.log(files[0].name);
  }

  public cargandoImagen($event){
    let files: FileList = document.querySelector('#customFileLang')['files'];
    console.log(files);
		this.enviandoImagen.postFileImagen(files[0]).subscribe(

			response => {
				this.respuestaImagenEnviada = response; 
				if(this.respuestaImagenEnviada <= 1){
					console.log("Error en el servidor"); 
				}else{

					if(this.respuestaImagenEnviada.code == 200 && this.respuestaImagenEnviada.status == "success"){

            this.resultadoCarga = 1;
            // console.log(this.resultadoCarga, this.respuestaImagenEnviada);
            document.querySelector('#nombreArchivo').textContent = '';
            this.router.navigate(['/controlPaginas']);
					}else{
            this.resultadoCarga = 2;
            // console.log(this.resultadoCarga, this.respuestaImagenEnviada);
            document.querySelector('#nombreArchivo').textContent = '';
            this.router.navigate(['/controlPaginas']);
					}

				}
			},
			error => {
				console.log('Esto es un error ',<any>error);
			}

		);//FIN DE METODO SUBSCRIBE

	}
}
