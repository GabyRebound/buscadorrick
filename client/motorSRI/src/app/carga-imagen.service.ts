
import { Injectable } from '@angular/core'; 
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CargaImagenService{

	public url_servidor = "http://localhost:3000/document-file";

	constructor(private http: HttpClient){}

	public postFileImagen(imagenParaSubir: File){

		const formData = new FormData(); 
		formData.append('file', imagenParaSubir, imagenParaSubir.name); 
		return this.http.post(this.url_servidor, formData);

	}

}