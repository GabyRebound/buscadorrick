import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PagesService {
  

  constructor(private http: HttpClient) { }

  buscarPaginas( palabraClave:string ){
    
    let paginasRes:Pagina[] = [];
    palabraClave = palabraClave.toLowerCase();
    
    // aqui hay que hacer la peticion al backend

    this.http.post("http://127.0.0.1:3000/search",{
      "token":  palabraClave,
    })
    .subscribe(
      data  => {

        console.log("POST Request is successful ", data);

        data['data'].documents.forEach(element => {

          paginasRes.push({
            similitud: element.sim,
            titulo: element.document.title,
            descripcion: element.document.description,
            palabraClave: element.document.tokens,
            nameDoc: element.document.root
          })
        });
      },
      error  => {
      console.log("Error", error);
      }
    );
    return paginasRes;
  }

  mostrarPaginas(){
    let paginasRes = [];
    this.http.get("http://127.0.0.1:3000/document")
    .subscribe( data  => {
        console.log("GET Request is successful ", data);
        data['data'].documents.forEach(element => {
          paginasRes.push({      
            similitud: element.sim,
            titulo: element.title,
            descripcion: element.description,
            palabraClave: element.tokens,
            nameDoc: element.root
          })
        });
      },
      error  => {
        console.log("Error", error);
      }
    );

    return paginasRes;
  }
}

export interface Pagina{
  similitud: string;
  titulo: string;
  descripcion: string;
  palabraClave: string;
  nameDoc: string;
  idx?: number;
}

